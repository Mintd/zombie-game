# The Plan (so far)

## Work that needs to be done

- Class diagram
  - Human
  - Zombie
  - WeaponItem
    - Zombie Arm -> Zombie Club
    - Zombie Leg -> Zombie Mace
  - Farmer
  - Food
- Interaction diagram
  - Human
    - Become a zombie
    - Attack Zombie
  - Zombie
    - Attack Humans
  - Farmer
    - Plant, harvest crops
- Design rationale
