package edu.monash.fit2099.interfaces;

/**
 * This interface provides the ability to add methods to Ground, without modifying code in the engine,
 * or downcasting references in the game.   
 */
public interface ItemInterface {
    /**
     * Indicates whether the item can be sold or not.
     */
    public enum Capable {FOR_SALE}

    /**
     * Ranking has a corresponding value of the item.
     */
    public enum Ranking {
        GOLD(9),
        SILVER(3),
        BRONZE(2),
        WOOD(1);

        /**
         * Value associated with the ranking.
         */
        public final int value;

        private Ranking(int value) {
            this.value = value;
        }
    }

    /**
     * Get the value corresponding to the ranking.
     *
     * @return integer value.
     */
    default int getRankingValue(){
        return 0;
    }

}
