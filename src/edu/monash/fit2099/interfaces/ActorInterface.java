package edu.monash.fit2099.interfaces;

/**
 * This interface provides the ability to add methods to Actor, without modifying code in the engine,
 * or downcasting references in the game.   
 */

public interface ActorInterface {

    /**
     * Get the credit points that the actor has.
     *
     * @return integer represents the credit point.
     */
    default int getCredit() {
        return 0;
    };

    /**
     * Set the credit point to a certain value.
     *
     * @param credit new credit point
     */
    default void setCredit(int credit){};

}
