package game;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.Exit;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Location;
import edu.monash.fit2099.engine.MoveActorAction;

/**
 * A class that generates either an FarmAction, HarvestAction, or MoveActorAction if the current
 * Actor is standing next to a patch of dirt, a growing crop, or a fully grown crop. If Actor is
 * undead, then it will return a FarmAction that will destroy the crop.
 */
public class FarmBehaviour implements Behaviour {
	private Random rand = new Random();

	/**
	 * Returns either a FarmAction or a HarvestAction depending on the surroundings.
	 *
	 * The surroundings are evaluated in this order:
	 * <p><ol>
	 * <li> If the Actor is a member of the undead team, and there is a crop next to or on it,
	 * return a FarmAction set to damage the crop.
	 * <li> If there is a harvestable crop next to or at the farmer, return a HarvestAction.
	 * <li> If there is a crop that is not fully grown at the farmer, return a FarmAction
	 * set to fertilize the crop.
	 * <li> If there is a crop next to the farmer, it will move towards it.
	 * <li> If there is dirt next to the farmer, then it will plant a Crop 33% of the time.
	 * </ol><p>
	 */
	@Override
	public Action getAction(Actor actor, GameMap map) {
		// assemble a list of Grounds at the Actor's location and at adjacent tiles
		List<Location> grounds = new ArrayList<>();
		grounds.add(map.locationOf(actor));
		map.locationOf(actor).getExits()
				.stream()
				.map(Exit::getDestination)
				.filter(loc -> loc.canActorEnter(actor))
				.forEachOrdered(grounds::add);

		// if Actor is a Zombie, it should harm the crop
		if (actor.hasCapability(ZombieCapability.UNDEAD)) {
			return grounds.stream()
					.filter(loc -> loc.getGround() instanceof Crop)
					.map(loc -> (Crop) loc.getGround())
					.filter(crop -> !crop.isDamaged())
					.findAny()
					.map(crop -> new FarmAction(crop, true))
					.orElse(null);
		}

		// harvest any crops that can be harvested
		Optional<Location> crop = grounds
				.stream()
				.filter(loc ->
					loc.getGround() instanceof Crop
					&& ((Crop) loc.getGround()).isFullyGrown()
				)
				.findAny();

		if (crop.isPresent()) {
			return new HarvestAction(crop.get());
		}

		// fertilize crop
		if (
				grounds.get(0).getGround() instanceof Crop
				&& !((Crop) grounds.get(0).getGround()).isDamaged()
		) {
			return new FarmAction((Crop) grounds.get(0).getGround(), false);
		}

		// move towards crop
		Optional<Location> destination = grounds
				.stream()
				.filter(loc -> loc.getGround() instanceof Crop)
				.filter(loc ->
						!((Crop) loc.getGround()).isDamaged() 
						|| ((Crop) loc.getGround()).isFullyGrown()
				)
				.findAny();

		if (destination.isPresent()) {
			return new MoveActorAction(destination.get(), "towards plant");
		}

		// plant crops
		Optional<Location> dirt = grounds
				.stream()
				.skip(1)
				.filter(loc -> loc.getGround() instanceof Dirt)
				.findFirst();

		if (dirt.isPresent() && rand.nextInt(3) == 0) {
			return new FarmAction(new Crop(), dirt.get());
		}

		return null;
	}
}
