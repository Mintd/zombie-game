package game;

/**
 * Class to represent points on the map as a pair of x and y coordinates.
 */
public class Point {
	/**
	 * x coordinate.
	 */
	public final int x;

	/**
	 * y coordinate.
	 */
	public final int y;

	/**
	 * Constructor.
	 * 
	 * @param x x coordinate.
	 * @param y y coordinate.
	 */
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
