package game;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import edu.monash.fit2099.engine.*;
import game.Ammunition.AmmoType;

/**
 * The main class for the zombie apocalypse game.
 *
 */
public class Application {
	private static final Ammunition[] ammunitions = new Ammunition[] {
		new Ammunition(AmmoType.SHOTGUN),
		new Ammunition(AmmoType.SHOTGUN),
		new Ammunition(AmmoType.SHOTGUN),
		new Ammunition(AmmoType.SNIPER),
		new Ammunition(AmmoType.SNIPER),
		new Ammunition(AmmoType.SNIPER),
	};

	public static void main(String[] args) {
		World world = new WorldPlus(new Display());
		FancyGroundFactory groundFactory = new FancyGroundFactory(new Dirt(), new Fence(), new Tree());
		Actor player = new Player("Player", '@', 100);


		GameMap compoundMap = initCompound(world, groundFactory, player);
		GameMap townMap = initTown(world, groundFactory, player);


		addVehicles(compoundMap, townMap);


		world.addPlayer(player, compoundMap.at(42, 15));
		world.run();
	}

	private static GameMap initCompound(World world, FancyGroundFactory groundFactory, Actor player) {
		int x, y;
		List<String> map = Arrays.asList(
		"................................................................................",
		"................................................................................",
		"....................................##########..................................",
		"..........................###########........#####..............................",
		"............++...........##......................########.......................",
		"..............++++.......#..............................##......................",
		".............+++...+++...#...............................#......................",
		".........................##..............................##.....................",
		"..........................#...............................#.....................",
		".........................##...............................##....................",
		".........................#...............................##.....................",
		".........................###..............................##....................",
		"...........................####......................######.....................",
		"..............................#########.........####............................",
		"............+++.......................#.........#...............................",
		".............+++++....................#.........#...............................",
		"...............++........................................+++++..................",
		".............+++....................................++++++++....................",
		"............+++.......................................+++.......................",
		"................................................................................",
		".........................................................................++.....",
		"........................................................................++.++...",
		".........................................................................++++...",
		"..........................................................................++....",
		"................................................................................");

		GameMap compoundMap = new GameMapPlus(groundFactory, map);
		world.addGameMap(compoundMap);


		// Place some random humans on compoundMap
		String[] humans = {"Carlton", "May", "Vicente", "Andrea", "Wendy",
				"Elina", "Winter", "Clem", "Jacob", "Jaquelyn"};

		for (String name : humans) {
			do {
				x = (int) Math.floor(Math.random() * 20.0 + 30.0);
				y = (int) Math.floor(Math.random() * 7.0 + 5.0);
			} 
			while (compoundMap.at(x, y).containsAnActor());
			
			if (name.equals("Carlton")) {
				compoundMap.at(x,  y).addActor(new Farmer(name));
			} else {
				compoundMap.at(x,  y).addActor(new Human(name));
			}
		}


		// place zombie on the map
		String[] zombies = {"Groan", "Boo", "Uuuurgh", "Mortalis", "Gaaaah", "Aaargh"};

		for (String name : zombies) {
			do {
				x = new Random().nextInt(compoundMap.getXRange().max());
				y = new Random().nextInt(compoundMap.getYRange().max());
			}
			while (compoundMap.at(x, y).containsAnActor());

			compoundMap.at(x, y).addActor(new Zombie(name));
		}


		// place ammunition on compoundMap at random locations
		for (int i = 0; i < 3; i++) {
			do {
				x = new Random().nextInt(compoundMap.getXRange().max());
				y = new Random().nextInt(compoundMap.getYRange().max());
			}
			while (! compoundMap.at(x, y).getGround().canActorEnter(player));

			for (Ammunition a: ammunitions) {
				compoundMap.at(x, y).addItem(a);
			}
		}


		// place a simple weapon
		compoundMap.at(74, 20).addItem(new Plank());


		return compoundMap;
	}

	private static GameMap initTown(World world, FancyGroundFactory groundFactory, Actor player) {
		int x, y;
		List<String> map = Arrays.asList(
		"................................................................................",
		"................................................................................",
		"................................................................................",
		"...........++..+...........................................#####................",
		".........+++++++.........................................##.....##..............",
		"............++.........................................###.......###............",
		".........+++..++..........................++.........####..........####.........",
		"........................................+++.....................................",
		"......................................+++.+++...................................",
		"................................................................................",
		"................................................................................",
		".............#####..............................................................",
		"............#....#..............................................................",
		"..........##.....###............................................................",
		"........###.........##..........................................................",
		"........#............###........................................................",
		"......##...............#..............................................+...+.....",
		"........#.............................................................+.+++.....",
		"......###..................##........................................++++.......",
		"........##.................##.........................####.............+........",
		".........##...............#.....................................................",
		"..........##.........#####.........................#..#.###....####.............",
		"..................................................###.##..####..................",
		"................................................###.............................",
		"................................................................................");

		GameMap townMap = new GameMap(groundFactory, map);
		world.addGameMap(townMap);


		// Place some random humans on townMap
		String[] humans = {"Charlie", "Mary", "Vic", "Andy", "William",
				"Eva", "Summer", "Clay", "Jacy", "Judy"};

		for (String name : humans) {
			do {
				x = (int) Math.floor(Math.random() * 20.0 + 15.0);
				y = (int) Math.floor(Math.random() * 9.0 + 5.0);
			}
			while (townMap.at(x, y).containsAnActor());

			if (name.equals("Charlie") || name.equals("Mary")) {
				townMap.at(x,  y).addActor(new Farmer(name));
			} else {
				townMap.at(x,  y).addActor(new Human(name));
			}
		}


		String[] zombies = {"Ruuu", "Foo", "Gruuu", "Moo", "Rarr", "Hru"};
		
		for (String name : zombies) {
			do {
				x = new Random().nextInt(townMap.getXRange().max());
				y = new Random().nextInt(townMap.getYRange().max());
			}
			while (townMap.at(x, y).containsAnActor());

			townMap.at(x, y).addActor(new Zombie(name));
		}
		

		// place ammunition on townMap at random locations
		for (int i = 0; i < 3; i++) {
			do {
				x = new Random().nextInt(townMap.getXRange().max());
				y = new Random().nextInt(townMap.getYRange().max());
			}
			while (! townMap.at(x, y).getGround().canActorEnter(player));

			for (Ammunition a: ammunitions) {
				townMap.at(x, y).addItem(a);
			}
		}


		// place a shotgun and a sniper rifle on townMap
		townMap.at(34, 12).addItem(new Shotgun());
		townMap.at(47, 11).addItem(new Sniper());

		// place a shop at the town map
		townMap.at(42, 15).addItem(new Shop());


		return townMap;
	}

	public static void addVehicles(GameMap compoundMap, GameMap townMap) {
		Vehicle vehicle;

		vehicle = new Vehicle(new MoveActorAction(townMap.at(42, 20), "to Town!"));
		compoundMap.at(42, 12).addItem(vehicle);

		vehicle = new Vehicle(new MoveActorAction(compoundMap.at(42, 12), "to Map!"));
		townMap.at(42, 20).addItem(vehicle);
	}
}
