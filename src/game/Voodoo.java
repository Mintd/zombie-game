package game;

import edu.monash.fit2099.engine.*;

/**
 * A Voodoo Priestess will appear on the map with 5 percent chance and able to do chanting that creates Zombies at random locations
 */
public class Voodoo extends ZombieActor {

    public static final int VANISH_TIME = 30;

    /**
     * Voodoo has a Vanish capability when counter reaches 30
     */
    public enum Capable {VANISH}

    /**
     * Records the number of play turns
     */
    private int counter = 0;

    /**
     * Represents number of times this action is being performed
     */
    private int copy = 0;

    /**
     * Creates a Voodoo that has APPEAR Capability.
     */
    public Voodoo() {
        super("Mambo Marie", 'M', 50, ZombieCapability.UNDEAD);
    }

    /**
     * Perform ChantAction every 10 turns, else perform WanderBehaviour.
     * When counter reaches 30, it will have VANISH capability.
     *
     * @param actions    collection of possible Actions for this Actor
     * @param lastAction The Action this Actor took last turn. Can do interesting things in conjunction with Action.getNextAction()
     * @param map        the map containing the Actor
     * @param display    the I/O object to which messages may be written
     * @return action to be performed
     */
    @Override
    public Action playTurn(Actions actions, Action lastAction, GameMap map, Display display) {
        Action action = new WanderBehaviour().getAction(this, map);
        action = action == null ? new DoNothingAction() : action;

        // increment the counter per play turn
        counter += 1;

        // if counter reaches 30 then add VANISH capability
        if (counter == VANISH_TIME) {
            addCapability(Capable.VANISH);
        }
        // if counter reaches 10 then perform ChantAction
        else if (counter % 10 == 0) {
            copy ++;
            action = new ChantAction(copy);
        }

        return action;
    }
}
