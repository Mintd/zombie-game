package game;

import edu.monash.fit2099.engine.Item;
import edu.monash.fit2099.engine.MoveActorAction;

/**
 * Vehicle is able to teleport the player to a different map
 */
public class Vehicle extends Item {

    /**
     * Constructor.
     * 
     * @param action MoveActorAction to the space on the new map.
     */
    public Vehicle(MoveActorAction action) {
        super("vehicle", 'v', false);
        this.allowableActions.add(action);
    }
}
