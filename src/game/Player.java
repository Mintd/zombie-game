package game;

import java.util.Comparator;
import java.util.Optional;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actions;
import edu.monash.fit2099.engine.Display;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Item;
import edu.monash.fit2099.engine.Menu;
import edu.monash.fit2099.engine.Weapon;

/**
 * Class representing the Player.
 */
public class Player extends Human {

	private Menu menu = new Menu();

	/**
	 * Credit point that the player has
	 */
	private int credit = 0;

	/**
	 * Constructor.
	 *
	 * @param name        Name to call the player in the UI
	 * @param displayChar Character to represent the player in the UI
	 * @param hitPoints   Player's starting number of hitpoints
	 */
	public Player(String name, char displayChar, int hitPoints) {
		super(name, displayChar, hitPoints);
	}

	/**
	 * Get the weapon this Actor is using.
	 * 
	 * If the current Actor is carrying weapons, returns the best one in the
	 * inventory. Otherwise, returns the Actor's natural fighting equipment e.g.
	 * fists.
	 *
	 * @return the Actor's weapon or natural
	 */
	@Override
	public Weapon getWeapon() {
		Optional<Weapon> weapon = inventory.stream()
				.filter(item -> item instanceof Weapon)
				.map(Item::asWeapon)
				.max(Comparator.comparing(Weapon::damage));

		if (weapon.isPresent()) {
			return weapon.get();
		} else {
			return getIntrinsicWeapon();
		}
	}

	@Override
	public void hurt(int points) {
		super.hurt(points);
		inventory.stream()
				.filter(item -> item instanceof Resettable)
				.map(item -> (Resettable) item)
				.forEach(Resettable::reset);
	}

	@Override
	public Action playTurn(Actions actions, Action lastAction, GameMap map, Display display) {
		Action action;

		actions.add(new QuitAction());

		// Handle multi-turn Actions
		if (lastAction.getNextAction() != null) {
			action = lastAction.getNextAction();
		} else {
			action = menu.showMenu(this, actions, display);
		}

		while (true) {
			if (action instanceof SubMenuAction) {
				SubMenuAction submenu = (SubMenuAction) action;

				action = submenu.showMenu(this, display, map);
			}

			if (action instanceof CancelAction) {
				action = menu.showMenu(this, actions, display);
			} else {
				break;
			}
		}

		return action;
	}


	/**
	 * Get the credit points that the actor has.
	 *
	 * @return integer represents the credit point.
	 */
	@Override
	public int getCredit() {
		return credit;
	}

	/**
	 * Set the credit point to a certain value.
	 *
	 * @param credit new credit point.
	 * @throws IllegalStateException if credit is below 0.
	 */
	@Override
	public void setCredit(int credit) {
		if (credit < 0) {
			throw new IllegalStateException("Credit set to below zero.");
		}
		this.credit = credit;
	}
}
