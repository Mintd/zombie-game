package game;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;

/**
 * Action representing a player wanting to quit the game.
 */
public class QuitAction extends Action {
	@Override
	public String execute(Actor actor, GameMap map) {
		throw new QuitException();
	}

	@Override
	public String menuDescription(Actor actor) {
		return "Quit Game";
	}
}
