package game;

import edu.monash.fit2099.engine.Actions;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.Exit;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Location;
import edu.monash.fit2099.engine.WeaponItem;

/**
 * Shotgun weapon.
 */
public class Shotgun extends WeaponItem implements SubMenu {
	public static final String SHOOT_VERB = "shoots";
	public static final int HIT_CHANCE = 75;
	public static final int DAMAGE_DEALT = 40;
	private static final Ranking RANKING = Ranking.GOLD;

	/**
	 * Constructor.
	 */
	public Shotgun() {
		super("shotgun", '﹃', 15, "whacks");
		addCapability(Capable.FOR_SALE);
		addCapability(RANKING);
	}

	@Override
	public Actions getMenuActions(Actor actor, GameMap map) {
		Location actorLocation = map.locationOf(actor);
		int xCurrent = actorLocation.x();
		int yCurrent = actorLocation.y();

		Actions actions = new Actions();

		if (
				actor.getInventory().stream()
						.filter(item -> item instanceof Ammunition)
						.map(item -> (Ammunition) item)
						.anyMatch(item -> item.forWeapon(this))
		) {
			actions.add(new CancelAction());
			actorLocation.getExits().stream()
				.map(Exit::getDestination)
				.map(target -> new ShotgunAimAction(
						this,
						ShotgunShootDirection.getDirection(
								target.x() - xCurrent,
								target.y() - yCurrent
						),
						xCurrent,
						yCurrent
				))
				.forEach(actions::add);
		} else {
			actions.add(new CancelAction("No ammunition"));
		}

		return actions;
	}

	@Override
	public void tick(Location currentLocation) {
		super.tick(currentLocation);
		allowableActions.clear();
	}

	@Override
	public void tick(Location currentLocation, Actor actor) {
		super.tick(currentLocation, actor);
		if (allowableActions.size() == 0) {
			allowableActions.add(new SubMenuAction(this, "Use shotgun"));
		}
	}

	/**
	 * Get the value corresponding to the ranking.
	 *
	 * @return integer value.
	 */
	@Override
	public int getRankingValue() {
		return RANKING.value;
	}
}
