package game;

import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.Exit;
import edu.monash.fit2099.engine.Location;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * Human corpse will become zombie
 */
public class Corpse extends PortableItem {
    private static final int THRESHOLD = 5;
    /**
     * Keep track of the number of calls to the {@code tick()} method
     */
    private int time = 0;

    private Random rand = new Random();
    
    /**
     * Indicates whether the Corpse can turn into a Zombie or not
     */
    private boolean turnZombie;

    /**
     * Constructor.
     * 
     * @param name the name of this Item
     * @param turnZombie true if and only if the attacking actor has {@code ZombieCapability.UNDEAD}
     */
    public Corpse(String name, boolean turnZombie) {
        super(name, 'C');
        this.turnZombie = turnZombie;
    }

    /**
     * Create the zombie at the random adjacent location
     *
     * @param currentLocation The location of the actor carrying this Item.
     * @param actor The actor carrying this Item.
     */
    @Override
    public void tick(Location currentLocation, Actor actor) {
        super.tick(currentLocation, actor);
        createZombie(currentLocation, actor);
    }

    /**
     * Create the zombie at the current location
     *
     * @param currentLocation The location of the ground on which we lie.
     */
    @Override
    public void tick(Location currentLocation) {
        super.tick(currentLocation);
        createZombie(currentLocation, null);
    }

    /**
     * Remove the corpse and create a zombie at either current or random adjacent location.
     * This depends on whether it is being carried by an actor or not.
     *
     * @param currentLocation The location of the corpse
     * @param actor The actor who is carrying th corpse, null if corpse is not carried by actor
     */
    private void createZombie(Location currentLocation, Actor actor) {
        if (turnZombie) {
            // only start trying to spawn Zombies once 5 turns have passed
            if (time >= THRESHOLD && (rand.nextInt(5) + 5) < time) {
                Location bodyLocation = currentLocation;

                if (bodyLocation.getActor() != null) {
                    // search the adjacent locations for a spot
                    List<Exit> exits = new ArrayList<>(currentLocation.getExits());
                    Collections.shuffle(exits);
                    Optional<Location> place = exits.stream()
                            .map(Exit::getDestination)
                            .filter(loc -> !loc.containsAnActor())
                            .findAny();

                    if (place.isPresent()) {
                        bodyLocation = place.get();
                    } else {
                        // no place to spawn new Zombie, postpone spawn and set time to 10 to make
                        // it check every turn
                        time = 10;
                        return;
                    }
                }

                // remove the corpse
                if (actor != null) {
                    actor.removeItemFromInventory(this);
                } else {
                    currentLocation.removeItem(this);
                }
                
                bodyLocation.addActor(new Zombie(this.toString()));
            }

            time++;
        }
    }
}
