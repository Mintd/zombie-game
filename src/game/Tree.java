package game;

/**
 * A tree that starts as a sapling and grows into a large tree.
 * 
 * @author ram
 *
 */
public class Tree extends Plant {
	/**
	 * Constructor.
	 */
	public Tree() {
		super(new int[] {0, 10, 20}, new char[] {'+', 't', 'T'});
	}
}
