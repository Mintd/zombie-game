package game;

import java.util.Arrays;

/**
 * Class representing a farmer.
 */
public class Farmer extends Human {
	private static final Behaviour[] farmerBehaviours = {
		new FarmBehaviour(),
	};

	/**
	 * Constructor.
	 *
	 * @param name Name of the farmer.
	 */
	public Farmer(String name) {
		super(name, 'F', 50);

		Behaviour[] tempBehaviours = Arrays.copyOf(
				farmerBehaviours, farmerBehaviours.length + behaviours.length
		);
		System.arraycopy(
				behaviours, 0, tempBehaviours, farmerBehaviours.length, behaviours.length
		);
		behaviours = tempBehaviours;
	}
}
