package game;

import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.GroundFactory;

import java.util.List;
import java.util.Random;

public class GameMapPlus extends GameMap {

    /**
     * GameMapPlus contains a Voodoo.
     */
    private static Actor voodoo = new Voodoo();

    /**
     * Constructor that creates a map from a sequence of ASCII strings.
     *
     * @param groundFactory Factory to create Ground objects
     * @param lines         List of Strings representing rows of the map
     */
    public GameMapPlus(GroundFactory groundFactory, List<String> lines) {
        super(groundFactory, lines);
    }

    /**
     * If current map does not contain Voodoo, it will appear with 5 percent chance if Voodoo is still conscious.
     * If Voodoo has VANISH capability, it will be removed from the current map.
     */
    @Override
    public void tick() {
        if (!contains(voodoo)) {
            // 5% chance of spawning Voodoo
            if (new Random().nextInt(20) == 0 && voodoo.isConscious()) {
                int x, y, counter;
                counter = 0;

                do {
                    if (counter > 100) {
                        // can't find a space, give up
                        super.tick();
                        return;
                    }

                    x = getXRange().min() + new Random().nextInt(Math.min(10, getXRange().max()));
                    y = getYRange().max() - new Random().nextInt(Math.min(10, getXRange().max()));
                    counter++;
                } while (at(x, y).containsAnActor());

                at(x, y).addActor(voodoo);
            }
        } else {
            // remove Voodoo from the map
            if (voodoo.hasCapability(Voodoo.Capable.VANISH)) {
                voodoo.removeCapability(Voodoo.Capable.VANISH);
                removeActor(voodoo);
            }
        }
        super.tick();
    }

    public boolean isVoodooDead() {
        return !voodoo.isConscious();
    }
}
