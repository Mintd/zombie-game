package game;

import edu.monash.fit2099.engine.*;


/**
 * PurchaseAction used up credit point and adds item purchased to actor's inventory and deduct the value point of the actor.
 */
public class PurchaseAction extends Action {

    private Shop shop;
    private Item item;

    /**
     * Constructor.
     *
     * @param shop Shop instance.
     * @param item Item to purchase.
     */
    public PurchaseAction(Shop shop, Item item) {
        this.shop = shop;
        this.item = item;
    }


    /**
     * Remove item from stock in shop and adds it to actor's inventory.
     *
     * @param actor The actor performing the action.
     * @param map The map the actor is on.
     * @return a description of the purchase and how much credit the user has left.
     */
    @Override
    public String execute(Actor actor, GameMap map) {

        // remove item from the stock
        shop.removeFromStock(item);
        // add item to actor's inventory
        actor.addItemToInventory(item);
        // decrement the credit points
        actor.setCredit(actor.getCredit() - item.getRankingValue());

        return String.format("%s purchases %s remain with %d credit point", actor, item, actor.getCredit());
    }

    @Override
    public String menuDescription(Actor actor) {
        return String.format("%s purchases %s with %d value", actor, item, item.getRankingValue());
    }
}
