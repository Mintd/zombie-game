package game;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;

/**
 * Action for Healing Actors.
 */
public class HealAction extends Action {
	private Food food;

	/**
	 * Constructor.
	 *
	 * @param food Food to consume.
	 */
	public HealAction(Food food) {
		this.food = food;
	}

	@Override
	public String execute(Actor actor, GameMap map) {
		actor.removeItemFromInventory(food);
		actor.heal(food.getHealAmount());

		return String.format(
				"%s eats %s, and heals for %d points.",
				actor, food, food.getHealAmount()
		);
	}

	@Override
	public String menuDescription(Actor actor) {
		return String.format("Player heals using %s", food);
	}
}
