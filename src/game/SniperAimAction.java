package game;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;

/**
 * Action for aiming the sniper at a target.
 */
public class SniperAimAction extends Action {
	private Sniper sniper;

	/**
	 * Constructor.
	 * 
	 * @param sniper Sniper the player is using.
	 */
	public SniperAimAction(Sniper sniper) {
		this.sniper = sniper;
	}

	@Override
	public String execute(Actor actor, GameMap map) {
		sniper.aim();
		return String.format("%s aims sniper", actor);
	}

	@Override
	public Action getNextAction() {
		return new SubMenuAction(sniper, "Use sniper");
	}

	@Override
	public String menuDescription(Actor actor) {
		return String.format("%s aims sniper", actor);
	}
}
