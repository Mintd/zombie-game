package game;

import edu.monash.fit2099.engine.WeaponItem;

/**
 * A mace made using Zombie legs.
 */
public class ZombieMace extends WeaponItem {

	private static final Ranking RANKING = Ranking.SILVER;

	/**
	 * Constructor.
	 */
	public ZombieMace() {
		super("zombie mace", '*', 25, "swings at");
		addCapability(Capable.FOR_SALE);
		addCapability(RANKING);
	}

	/**
	 * Get the value corresponding to the ranking.
	 *
	 * @return integer value.
	 */
	@Override
	public int getRankingValue() {
		return RANKING.value;
	}
}
