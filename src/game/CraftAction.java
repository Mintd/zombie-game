package game;

import edu.monash.fit2099.engine.*;


/**
 * Craft the weapon to other kinds of weapons
 */
public class CraftAction extends Action {

    private Item zombieLimb;

    /**
     * Craft the zombieLimb into its corresponding weapon
     *
     * @param zombieLimb ZombieLimb instance
     */
    public CraftAction(Item zombieLimb) {
        this.zombieLimb = zombieLimb;
    }

    /**
     * Craft the ZombieLimb into ZombieClub and ZombieMace
     *
     * @param actor The actor performing the action
     * @param map The map the actor is on
     * @return string result indicates if actor performs the CraftAction or not
     */
    @Override
    public String execute(Actor actor, GameMap map) {
        String result = "";

        // remove the zombie limb from inventory
        actor.removeItemFromInventory(zombieLimb);
        // get the item that the zombie limb is crafted into
        Item weaponItem = ((ZombieLimb) zombieLimb).craft();
        // add the new weapon to inventory
        actor.addItemToInventory(weaponItem);

        result += String.format("%s crafts %s into %s.", actor, zombieLimb, weaponItem);

        return result;
    }

    /**
     * Return a description that the weapon the actor craft into
     *
     * @param actor The actor performing the action
     * @return string description
     */
    @Override
    public String menuDescription(Actor actor) {
        return String.format("%s crafts zombie limb", actor);
    }
}
