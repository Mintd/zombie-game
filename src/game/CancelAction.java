package game;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;

/**
 * Action representing the user exiting from a submenu.
 */
public class CancelAction extends Action {
	private String description = "Cancel";

	/**
	 * Empty constructor. Description will be "Cancel".
	 */
	public CancelAction() {}

	/**
	 * Constructor. Use this to add a custom description.
	 * 
	 * @param description description to show.
	 */
	public CancelAction(String description) {
		this.description = description;
	}

	@Override
	public String execute(Actor actor, GameMap map) {
		throw new IllegalStateException("CancelAction's execute method called.");
	}

	@Override
	public String menuDescription(Actor actor) {
		return description;
	}
}
