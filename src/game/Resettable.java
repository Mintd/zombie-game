package game;

/**
 * Interface for objects that store state that need to be reset.
 */
public interface Resettable {
	/**
	 * Reset object to original state.
	 */
	public void reset();
}
