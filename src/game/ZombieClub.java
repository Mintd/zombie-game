package game;

import edu.monash.fit2099.engine.WeaponItem;

/**
 * A club made using Zombie arms.
 */
public class ZombieClub extends WeaponItem {

	private static final Ranking RANKING = Ranking.SILVER;

	/**
	 * Constructor.
	 */
	public ZombieClub() {
		super("zombie club", '|', 25, "swings at");
		addCapability(Capable.FOR_SALE);
		addCapability(RANKING);
	}

	/**
	 * Get the value corresponding to the ranking.
	 *
	 * @return integer value.
	 */
	@Override
	public int getRankingValue() {
		return RANKING.value;
	}
}
