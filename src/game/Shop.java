package game;

import edu.monash.fit2099.engine.*;
import game.Ammunition.AmmoType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Shop sells Shotgun, Sniper, Ammunition.
 */
public class Shop extends Item implements SubMenu {

    private List<Item> stock = new ArrayList<>(Arrays.asList(
        new Shotgun(),
        new Shotgun(),
        new Shotgun(),
        new Sniper(),
        new Sniper(),
        new Sniper(),
        new Ammunition(AmmoType.SHOTGUN),
        new Ammunition(AmmoType.SHOTGUN),
        new Ammunition(AmmoType.SHOTGUN),
        new Ammunition(AmmoType.SHOTGUN),
        new Ammunition(AmmoType.SHOTGUN),
        new Ammunition(AmmoType.SHOTGUN),
        new Ammunition(AmmoType.SHOTGUN),
        new Ammunition(AmmoType.SNIPER),
        new Ammunition(AmmoType.SNIPER),
        new Ammunition(AmmoType.SNIPER),
        new Ammunition(AmmoType.SNIPER),
        new Ammunition(AmmoType.SNIPER),
        new Ammunition(AmmoType.SNIPER),
        new Ammunition(AmmoType.SNIPER)
    ));


    /***
     * Constructor to create a Shop.
     */
    public Shop() {
        super("shop", '⌂', false);
    }

    /**
     * Returns Actions containing CancelAction, SellAction, PurchaseAction.
     *
     * @param actor actor
     * @param map   map
     * @return actions that the actor can perform
     */
    @Override
    public Actions getMenuActions(Actor actor, GameMap map) {
        Actions actions = new Actions();

        if (stock.isEmpty()) {
            actions.add(new CancelAction("Shop out of stock"));
            return actions;
        }

        // actor can cancel selling or purchasing
        actions.add(new CancelAction());
        // actor can sell if it has weapon for sale
        if (
                actor.getInventory()
                        .stream()
                        .anyMatch(item -> item.hasCapability(Capable.FOR_SALE))
        ) {
            for (Ranking ranking : Ranking.values()) {
                actor.getInventory()
                        .stream()
                        .filter(item -> item.hasCapability(Capable.FOR_SALE))
                        .filter(item -> item.hasCapability(ranking))
                        .map(SellAction::new)
                        .forEach(actions::add);
            }
        } else {
            actions.add(new CancelAction("No item for sale"));
        }

        List<Action> potentialPurchases = stock.stream()
                .filter(item -> actor.getCredit() >= item.getRankingValue())
                .map(item -> new PurchaseAction(this, item))
                .collect(Collectors.toList());

        if (potentialPurchases.isEmpty()) {
            actions.add(new CancelAction("Not enough credit to purchase"));
        } else {
            actions.add(potentialPurchases);
        }

        return actions;
    }

    /**
     * Add SubMenuAction if Player is standing at the shop location.
     * Otherwise, clear the allowableActions.
     *
     * @param currentLocation The location of the ground on which we lie.
     */
    @Override
    public void tick(Location currentLocation) {
        super.tick(currentLocation);

        if (currentLocation.getActor() instanceof Player) {
            if (allowableActions.size() == 0) {
                allowableActions.add(new SubMenuAction(this, "Enter Shop"));
            }
        } else {
            allowableActions.clear();
        }
    }

    /**
     * Remove an Item from the shop's stock.
     * 
     * @param item Item to remove.
     */
    public void removeFromStock(Item item) {
        stock.remove(item);
    }
}
