package game;

import edu.monash.fit2099.engine.WeaponItem;

/**
 * A primitive weapon.
 * 
 * @author ram
 *
 */
public class Plank extends WeaponItem {

    private static final Ranking RANKING = Ranking.WOOD;

    public Plank() {
		super("plank", ')', 20, "whacks");
		addCapability(Capable.FOR_SALE);
		addCapability(RANKING);
	}

	/**
	 * Get the value corresponding to the ranking.
	 *
	 * @return integer value.
	 */
	@Override
	public int getRankingValue() {
		return RANKING.value;
	}
}
