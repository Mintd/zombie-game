package game;

import java.util.stream.StreamSupport;

import edu.monash.fit2099.engine.Display;
import edu.monash.fit2099.engine.World;

/**
 * An extended World class that can do more.
 *
 * {@inheritDoc}
 */
public class WorldPlus extends World {
	protected enum GameState {RUNNING, USER_QUIT, PLAYER_LOSE, PLAYER_WIN}
	protected GameState currentState;

	/**
	 * Constructor.
	 *
	 * @param display the Display that will display this World.
	 */
	public WorldPlus(Display display) {
		super(display);
		currentState = GameState.RUNNING;
	}

	@Override
	public void run() {
		try {
			super.run();
		} catch (QuitException e) {
			currentState = GameState.USER_QUIT;
			display.println(endGameMessage());
		}
	}

	/**
	 * Returns true if the game is still running.
	 *
	 * The game is considered to still be running if:
	 * <p><ul>
	 * <li> the player is still around.
	 * <li> there are still humans on the map.
	 * <li> Mambo Marie is still alive.
	 * <li> there are still zombies on the map.
	 * </ul><p>
	 *
	 * @return true if the above conditions are met.
	 */
	@Override
	protected boolean stillRunning() {
		if (
				gameMaps.stream()
						.filter(map -> map instanceof GameMapPlus)
						.map(map -> (GameMapPlus) map)
						.findFirst()
						.orElseThrow(() -> new IllegalStateException("No GameMapPlus in World"))
						.isVoodooDead()

				&& StreamSupport.stream(actorLocations.spliterator(), false)
						.noneMatch(actor -> actor.hasCapability(ZombieCapability.UNDEAD))
		) {
			currentState = GameState.PLAYER_WIN;
		} else if (
				!actorLocations.contains(player)
				|| StreamSupport.stream(actorLocations.spliterator(), false)
						.filter(actor -> !(actor instanceof Player))
						.filter(actor -> 
								actorLocations.locationOf(actor).map() instanceof GameMapPlus
						)
						.noneMatch(actor -> actor instanceof Human)
		) {
			currentState = GameState.PLAYER_LOSE;
		}

		return currentState == GameState.RUNNING;
	}

	/**
	 * Return a string that can be displayed when the game ends.
	 *
	 * @return game end message
	 * @throws IllegalStateException if called when currentState is not an endgame state.
	 */
	@Override
	protected String endGameMessage() {
		switch (currentState) {
			case USER_QUIT: return "Goodbye";
			case PLAYER_LOSE: return "Game Over";
			case PLAYER_WIN: return "Congratulations! You won!";
			default: throw new IllegalStateException(String.format(
					"endGameMessage() called when currentState = %s", currentState
			));
		}
	}
}
