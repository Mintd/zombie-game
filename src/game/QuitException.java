package game;

/**
 * Exception for when the player quits the game.
 */
public class QuitException extends RuntimeException {
}
