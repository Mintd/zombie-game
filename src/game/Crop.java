package game;

import edu.monash.fit2099.engine.Actions;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.Location;

/**
 * Class that represents a crop that can be planted by Farmers.
 */
public class Crop extends Plant {
	private final Actions actions = new Actions();
	private final Food produce;
	private boolean damaged = false;

	/**
	 * Constructor.
	 */
	public Crop() {
		super(new int[] {0, 20}, new char[] {'p', 'P'});

		this.produce = new Food(30);
	}

	/**
	 * Gets the plant's produce.
	 *
	 * @return Produce of the plant.
	 * @throws IllegalStateException If called when the plant is not fully grown.
	 */
	public Food harvest() {
		if (isFullyGrown()) {
			return produce;
		} else {
			throw new IllegalStateException("Plant is not ready for harvesting.");
		}
	}

	/**
	 * Fertilizes the plant.
	 */
	public void fertilize() {
		if (damaged) {
			throw new IllegalStateException("Fertilize called when damaged");
		}

		grow(10);
	}

	/**
	 * Gets the state of the plant.
	 * 
	 * @return whether the plant is damaged, preventing fertilization.
	 */
	public boolean isDamaged() {
		return damaged;
	}

	/**
	 * Damages the plant, preventing fertilization.
	 */
	public void damage() {
		if (damaged) {
			throw new IllegalStateException("Damaged called when already damaged");
		}

		damaged = true;
	}

	@Override
	public void tick(Location location) {
		super.tick(location);

		if (isFullyGrown() && actions.size() == 0) {
			actions.add(new HarvestAction(location));
		}
	}

	@Override
	public Actions allowableActions(Actor actor, Location location, String direction) {
		return actions;
	}
}
