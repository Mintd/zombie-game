package game;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Location;

/**
 * Action for harvesting Crops.
 */
public class HarvestAction extends Action {
	private Crop target;
	private Location location;

	/**
	 * Constructor.
	 * @param location Location of the Crop.
	 */
	public HarvestAction(Location location) {
		this.location = location;
		this.target = (Crop) location.getGround();
	}

	@Override
	public String execute(Actor actor, GameMap map) {
		location.setGround(new Dirt());

		if (actor instanceof Player) {
			actor.addItemToInventory(target.harvest());
		} else {
			location.addItem(target.harvest());
		}

		return String.format("%s harvests %s", actor, target);
	}

	@Override
	public String menuDescription(Actor actor) {
		return String.format("%s harvests %s", actor, target);
	}
}
