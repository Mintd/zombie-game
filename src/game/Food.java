package game;

import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.Location;

/**
 * Class representing a food that can be eaten.
 */
public class Food extends PortableItem {
	private final int healAmount;

	/**
	 * Constructor.
	 *
	 * Creates a new Food with the provided characteristics.
	 *
	 * @param healAmount The amount that the food should heal by.
	 */
	public Food(int healAmount) {
		super("Food", 'f');
		this.healAmount = healAmount;
	}

	/**
	 * Detects if the item is being carried by an Actor.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void tick(Location currentLocation, Actor actor) {
		if (allowableActions.size() == 0) {
			allowableActions.add(new HealAction(this));
		}
	}

	/**
	 * Detects if the item is on the Ground.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void tick(Location currentLocation) {
		if (allowableActions.size() == 1) {
			allowableActions.clear();
		}
	}

	/**
	 * Gets the amount that this food will heal by.
	 *
	 * @return Amount of healing.
	 */
	public int getHealAmount() {
		return healAmount;
	}
}
