package game;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Location;

/**
 * Action for performing farming.
 */
public class FarmAction extends Action {
	private Crop crop;
	private Location location;
	private final String formatString;
	private boolean damageCrop;

	/**
	 * Constructor.
	 *
	 * This FarmAction fertilizes the Crop.
	 * 
	 * @param crop the Crop to fertilize.
	 * @param damageCrop whether to damage the crop.
	 */
	public FarmAction(Crop crop, boolean damageCrop) {
		this.crop = crop;
		this.formatString = damageCrop ? "%s damages %s" : "%s fertilizes %s";
		this.damageCrop = damageCrop;
	}

	/**
	 * Constructor.
	 *
	 * This FarmAction plants the Crop at the specified location.
	 * 
	 * @param crop Crop to plant.
	 * @param location Where to plant the Crop.
	 */
	public FarmAction(Crop crop, Location location) {
		this.crop = crop;
		this.location = location;
		this.formatString = "%s plants %s";
	}

	@Override
	public String execute(Actor actor, GameMap map) {
		if (damageCrop) {
			crop.damage();
		} else if (location != null) {
			location.setGround(crop);
		} else {
			crop.fertilize();
		}

		return String.format(formatString, actor, crop);
	}

	@Override
	public String menuDescription(Actor actor) {
		return String.format(formatString, actor, crop);
	}
}
