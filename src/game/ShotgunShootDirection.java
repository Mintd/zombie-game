package game;

import java.util.Arrays;

/**
 * Enum representing directions a shotgun could be shot.
 */
public enum ShotgunShootDirection {
	N(
			"North",
			0,
			-1,
			new Point[] {
					new Point(0, -1),
					new Point(-1, -2),
					new Point(0, -2),
					new Point(1, -2),
					new Point(-2, -3),
					new Point(-1, -3),
					new Point(0, -3),
					new Point(1, -3),
					new Point(2, -3),
			}
	),
	NE(
			"North-East",
			1,
			-1,
			new Point[] {
					new Point(1, -1),
					new Point(2, -1),
					new Point(3, -1),
					new Point(1, -2),
					new Point(2, -2),
					new Point(3, -2),
					new Point(1, -3),
					new Point(2, -3),
					new Point(3, -3),
			}
	),
	E(
			"East",
			1,
			0,
			new Point[] {
					new Point(1, 0),
					new Point(2, -1),
					new Point(2, 0),
					new Point(2, 1),
					new Point(3, -2),
					new Point(3, -1),
					new Point(3, 0),
					new Point(3, 1),
					new Point(3, 2),
			}
	),
	SE(
			"South-East",
			1,
			1,
			new Point[] {
					new Point(1, 1),
					new Point(1, 2),
					new Point(1, 3),
					new Point(2, 1),
					new Point(2, 2),
					new Point(2, 3),
					new Point(3, 1),
					new Point(3, 2),
					new Point(3, 3),
			}
	),
	S(
			"South",
			0,
			1,
			new Point[] {
					new Point(0, 1),
					new Point(-1, 2),
					new Point(0, 2),
					new Point(1, 2),
					new Point(-2, 3),
					new Point(-1, 3),
					new Point(0, 3),
					new Point(1, 3),
					new Point(2, 3),
			}
	),
	SW(
			"South-West",
			-1,
			1,
			new Point[] {
					new Point(-1, 1),
					new Point(-1, 2),
					new Point(-1, 3),
					new Point(-2, 1),
					new Point(-2, 2),
					new Point(-2, 3),
					new Point(-3, 1),
					new Point(-3, 2),
					new Point(-3, 3),
			}
	),
	W(
			"West",
			-1,
			0,
			new Point[] {
					new Point(-1, 0),
					new Point(-2, 1),
					new Point(-2, 0),
					new Point(-2, -1),
					new Point(-3, 2),
					new Point(-3, 1),
					new Point(-3, 0),
					new Point(-3, -1),
					new Point(-3, -2),
			}
	),
	NW(
			"North-West",
			-1,
			-1,
			new Point[] {
					new Point(-1, -1),
					new Point(-1, -2),
					new Point(-1, -3),
					new Point(-2, -1),
					new Point(-2, -2),
					new Point(-2, -3),
					new Point(-3, -1),
					new Point(-3, -2),
					new Point(-3, -3),
			}
	);

	/**
	 * User friendly name for the direction.
	 */
	public final String fullName;

	/**
	 * The change in position of one tile from the player.
	 */
	public final int xChange, yChange;
	private final Point[] changes;

	private ShotgunShootDirection(String fullName, int xChange, int yChange, Point[] changes) {
		this.fullName = fullName;
		this.xChange = xChange;
		this.yChange = yChange;
		this.changes = changes;
	}

	/**
	 * Get the ShotgunShootDirection that corresponds with the direction of the change relative to
	 * the player.
	 * 
	 * @param xChange x change relative to the player.
	 * @param yChange y change relative to the player.
	 * @return direction that matches the x and y change.
	 */
	public static ShotgunShootDirection getDirection(int xChange, int yChange) {
		return Arrays.stream(ShotgunShootDirection.values())
				.filter(direction -> direction.xChange == xChange && direction.yChange == yChange)
				.findAny()
				.orElseThrow(() -> new IllegalStateException(
						String.format("Unknown direction: %d, %d", xChange, yChange)
				));
	}

	/**
	 * Get the coordinates of all {@link edu.monash.fit2099.engine.Location}s that <b>could</b> be affected by the shotgun.
	 * 
	 * @param x x coordinate of the player.
	 * @param y y coordinate of the player.
	 * @return {@link Point}s on the map that <b>could</b> be affected by the shotgun blast.
	 */
	public Point[] getAffectedLocations(int x, int y) {
		return Arrays.stream(this.changes)
				.map(point -> new Point(x + point.x, y + point.y))
				.toArray(Point[]::new);
	}
}
