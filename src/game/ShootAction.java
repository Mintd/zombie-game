package game;

import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Weapon;

/**
 * Action for shooting targets. Mostly a wrapper around AttackAction.
 */
public class ShootAction extends AttackAction {
	private boolean consumeAmmo;

	/**
	 * Constructor.
	 * 
	 * @param target the Actor to attack.
	 * @param weapon weapon to use for this attack.
	 * @param hitChance chance for attack to hit the target.
	 * @param damage amount of damage to deal to the target.
	 * @param verb verb to use to describe the attack.
	 */
	public ShootAction(Actor target, Weapon weapon, int hitChance, int damage, String verb) {
		super(target);
		this.weapon = weapon;
		this.hitChance = hitChance;
		this.damage = damage;
		this.verb = verb;
		this.consumeAmmo = true;
	}

	/**
	 * Constructor.
	 * 
	 * @param target the Actor to attack.
	 * @param weapon weapon to use for this attack.
	 * @param hitChance chance for attack to hit the target.
	 * @param damage amount of damage to deal to the target.
	 * @param verb verb to use to describe the attack.
	 * @param consumeAmmo controls whether this shot consumes ammunition.
	 */
	public ShootAction(
			Actor target,
			Weapon weapon,
			int hitChance,
			int damage,
			String verb,
			boolean consumeAmmo
	) {
		this(target, weapon, hitChance, damage, verb);
		this.consumeAmmo = consumeAmmo;
	}

	@Override
	public String execute(Actor actor, GameMap map) {
		if (consumeAmmo) {
			actor.removeItemFromInventory(
					actor.getInventory().stream()
							.filter(item -> item instanceof Ammunition)
							.filter(item -> ((Ammunition) item).forWeapon(weapon))
							.findFirst()
							.orElseThrow(() -> new IllegalStateException("Ammo not found"))
			);
		}

		return super.execute(actor, map);
	}

	@Override
	public String menuDescription(Actor actor) {
		return String.format("%s shoots %s", actor, target);
	}
}
