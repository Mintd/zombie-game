package game;

import edu.monash.fit2099.engine.*;

/**
 * Sell the item to the Shop to get credit points.
 */
public class SellAction extends Action {

    private Item item;
    private int credit;

    /**
     * Constructor.
     *
     * @param item   Item that the actor can sell.
     */
    public SellAction(Item item) {
        this.item = item;
        this.credit = item.getRankingValue();
    }

    /**
     * Adds credit points to the Actor and removes the Weapon from Actor's inventory and from the map
     *
     * @param actor The actor performing the action.
     * @param map   The map the actor is on.
     * @return string description of the action
     */
    @Override
    public String execute(Actor actor, GameMap map) {
        // Add credit points to the actor
        actor.setCredit(actor.getCredit() + credit);
        // Remove item from inventory
        actor.removeItemFromInventory(item);
        // Remove item from the map
        map.locationOf(actor).removeItem(item);

        return menuDescription(actor);
    }

    @Override
    public String menuDescription(Actor actor) {
        return String.format("%s sells %s for %d credit points. %s has %d credit", actor, item, credit, actor, actor.getCredit());
    }
}
