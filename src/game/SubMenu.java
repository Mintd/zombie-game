package game;

import edu.monash.fit2099.engine.Actions;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;

/**
 * Interface marking that the object can be interacted with through submenus.
 */
public interface SubMenu {
	/**
	 * Get the actions necessary for the submenu.
	 * 
	 * @param actor actor opening the submenu.
	 * @param map map the actor is on.
	 * @return actions to show in the submenu.
	 */
	public Actions getMenuActions(Actor actor, GameMap map);
}
