package game;

import edu.monash.fit2099.engine.*;

import java.util.List;
import java.util.Random;

/**
 * Limb that belongs to Zombie's inventory before it is dropped onto the ground
 */
public class PortableLimbItem extends PortableItem {

    /**
     * Constructor.
     *
     * @param name name of the limb
     * @param displayChar display character
     */
    public PortableLimbItem(String name, char displayChar) {
        super(name, displayChar);
    }

    /**
     * Get the DropLimbItemAction that performs the drop action specific to the PortableLimbItem.
     *
     * @return DropLimbItemAction
     */
    @Override
    public DropItemAction getDropAction() {
        return new DropLimbItemAction(this);
    }

    /**
     * Special action that is specific to PortableLimbItem only.
     */
    private class DropLimbItemAction extends DropItemAction {

        /**
         * Constructor.
         *
         * @param item the item to drop
         */
        public DropLimbItemAction(Item item) {
            super(item);
        }

        /**
         * Remove PortableLimbItem from Zombie's inventory.
         * Add ZombieLimb WeaponItem to the random adjacent location.
         *
         * @param actor The actor performing the action
         * @param map The map the actor is on
         * @return
         */
        @Override
        public String execute(Actor actor, GameMap map) {
            actor.removeItemFromInventory(item);
            ZombieLimb zombieLimb = new ZombieLimb(
                    item.toString().equals("arm") ? ZombieLimb.Limb.ARM : ZombieLimb.Limb.LEG);
            // add ZombieLimb to random adjacent location
            List<Exit> randomExit = map.locationOf(actor).getExits();
            randomExit
                    .get(new Random().nextInt(randomExit.size()))
                    .getDestination()
                    .addItem(zombieLimb);
            return super.menuDescription(actor);
        }
    }
}
