package game;

import java.util.*;
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import edu.monash.fit2099.engine.*;
import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actions;
import edu.monash.fit2099.engine.Display;
import edu.monash.fit2099.engine.DoNothingAction;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.IntrinsicWeapon;
import edu.monash.fit2099.engine.Weapon;

/**
 * A Zombie.
 * 
 * This Zombie is pretty boring.  It needs to be made more interesting.
 * 
 * @author ram
 *
 */
public class Zombie extends ZombieActor {

	/** Zombie's possible capabilities */
	public enum Capable {LIMB_LOSS, SPEED_DECREASE, FROZEN}

	public static final List<String> utterances = Collections.unmodifiableList(
			Arrays.asList("Braaaaains", "Aaargh", "Uuurgh")
	);

	private Behaviour[] behaviours = {
			new FarmBehaviour(),
			new PickUpItemBehaviour(Weapon.class, 1),
			new AttackBehaviour(ZombieCapability.ALIVE),
			new HuntBehaviour(Human.class, 10),
			new WanderBehaviour()
	};

	private Random rand = new Random();
	private int punchChance = 50;
	/** Detects whether the Zombie has move turn previously or not */
	private boolean moveTurn = false;

	/**
	 * Creates a Zombie and adds limbs to its inventory.
	 *
	 * @param name name of the Zombie
	 */
	public Zombie(String name) {
		super(name, 'Z', 100, ZombieCapability.UNDEAD);
		// Add arms and legs to Zombie's inventory
		List<Item> limbs = new ArrayList<Item>() {{
			add(new PortableLimbItem("arm", ']'));
			add(new PortableLimbItem("arm", ']'));
			add(new PortableLimbItem("leg", ']'));
			add(new PortableLimbItem("leg", ']'));

		}};
		inventory.addAll(limbs);
	}


	/**
	 * Return either punch or bite IntrinsicWeapon.
	 * If Zombie has one arm left, reduce the chance of returning punch.
	 * If Zombie has no arm left, it will not be able to have punch IntrinsicWeapon.
	 *
	 * @return IntrinsicWeapon instance
	 */
	@Override
	public IntrinsicWeapon getIntrinsicWeapon() {
		// punch chance reduce by half if only one arm left
		if (countLimb("arm") == 1)
			punchChance /= 2;
		else if (countLimb("arm") == 0)
			punchChance = 0;

		if (rand.nextInt(100) < punchChance) {
			return new IntrinsicWeapon(10, "punches");
		} else {
			return new IntrinsicWeapon(5, "bites");
		}
	}

	/**
	 * If a Zombie can attack, it will.  If not, it will chase any human within 10 spaces.  
	 * If no humans are close enough it will wander randomly.
	 * Perform drop limb if Zombie has the capability of LIMB_LOSS.
	 * 
	 * @param actions list of possible Actions
	 * @param lastAction previous Action, if it was a multiturn action
	 * @param map the map where the current Zombie is
	 * @param display the Display where the Zombie's utterances will be displayed
	 */
	@Override
	public Action playTurn(Actions actions, Action lastAction, GameMap map, Display display) {
		if (rand.nextInt(10) == 0) {
			display.println(String.format(
				"%s: %s",
				this.name,
				utterances.get(rand.nextInt(utterances.size()))
			));
		}

		// Get the drop action for the limb to be dropped
		if (hasCapability(Capable.LIMB_LOSS)) {
			Optional<Item> limb = inventory
					.stream()
					.filter(item -> item instanceof PortableLimbItem)
					.skip(rand.nextInt(inventory.size()))
					.findFirst();
			if (limb.isPresent()) {
				removeCapability(Capable.LIMB_LOSS);
				// prohibit to move if loses the only leg else speed decrease
				if (limb.get().toString().equals("leg")) {
					if (countLimb("leg") == 0)
						addCapability(Capable.FROZEN);
					else
						addCapability(Capable.SPEED_DECREASE);
				}
				// drop weapon if loses arm(s)
				if (limb.get().toString().equals("arm")) {
					dropWeapon(map);
				}
				return limb.get().getDropAction();
			}
		}

		for (Behaviour behaviour : behaviours) {
			if (satisfiedCondition(behaviour)) {
				Action action = behaviour.getAction(this, map);
				if (action != null)
					return action;
			}
			if (behaviour instanceof HuntBehaviour) {
			    // invert the move turn to set to the original value
                moveTurn = ! moveTurn;
            }
		}
		return new DoNothingAction();	
	}

	/**
	 * Capable of dropping limb when it gets hurt with 25 percent chance
	 *
	 * @param points number of hitpoints to deduct.
	 */
	@Override
	public void hurt(int points) {
		super.hurt(points);
		if (rand.nextInt(100) < 25 && limbRemain() != 0) {
			addCapability(Capable.LIMB_LOSS);
		}
	}

	/**
	 * Count the number of the specified limb which the Zombie has left
	 *
	 * @param name name of the limb
	 * @return number of limbs
	 */
	private int countLimb(String name) {
		int count;
		try {
			count = (int) getInventory()
					.stream()
					.filter(limb -> limb.toString().equals(name))
					.count();
		} catch (NullPointerException e) {
			count = 0;
		}

		return count;
	}

	/**
	 * Count the remaining limb of the zombie
	 *
	 * @return number of limbs left
	 */
	private int limbRemain() {
		return countLimb("arm") + countLimb("leg");
	}

	/**
	 * Drop the weapon if loses one arm with 50 percent chance else drop all the weapons
	 *
	 * @param map the map where the current Zombie is
	 */
	private void dropWeapon(GameMap map) {
		// get the weapons in inventory
		List<Item> weapons = inventory
				.stream()
				.filter(weapon -> weapon instanceof WeaponItem)
				.collect(Collectors.toList());
		// if there's weapon to drop
		if (!weapons.isEmpty()) {
            // with 50 percent chance of dropping if still has one arm left else drop all the weapons
			if (countLimb("arm") > 0 && rand.nextInt(10) < 5) {
				weapons.get(0).getDropAction().execute(this, map);
			}
			else {
				for (Item weapon: weapons) {
					weapon.getDropAction().execute(this, map);
				}
			}
		}
	}

	/**
	 * Checks whether the behaviour has satisfied the condition to perform the action
	 *
	 * @param behaviour	behaviour of the Zombie
	 * @return true if satisfied, false otherwise
	 */
	private boolean satisfiedCondition(Behaviour behaviour) {
		if (behaviour instanceof PickUpItemBehaviour || behaviour instanceof FarmBehaviour) {
			return countLimb("arm") != 0; // false if no arm
		} else if (behaviour instanceof HuntBehaviour || behaviour instanceof WanderBehaviour) {
			if (countLimb("leg") == 0) {
				return false; // false if no leg
			}
			
			if (hasCapability(Capable.SPEED_DECREASE)) {
                moveTurn = !moveTurn; // negate the previous result
                return moveTurn;
            }
		}
		return true;
	}

}
