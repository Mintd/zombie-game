package game;

import edu.monash.fit2099.engine.Actions;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Location;
import edu.monash.fit2099.engine.WeaponItem;

/**
 * A sniper rifle that can shoot any enemy present on the map.
 */
public class Sniper extends WeaponItem implements SubMenu, Resettable {
	private static final String SHOOT_VERB = "shoots";
	private static final int MAX_ACCURACY = 2;
	private static final Ranking RANKING = Ranking.GOLD;
	private int accuracyLevel = 0;
	private int chaser = 0;

	/**
	 * Constructor.
	 */
	public Sniper() {
		super("sniper", 'S', 15, "whacks");
		addCapability(Capable.FOR_SALE);
		addCapability(RANKING);
	}

	/**
	 * Aim the sniper.
	 */
	public void aim() {
		accuracyLevel += 1;
	}

	@Override
	public Actions getMenuActions(Actor actor, GameMap map) {
		int hitChance;
		int damage = 0;

		switch (accuracyLevel) {
			case 0:
				hitChance = 75;
				damage = 30;
				break;

			case 1:
				hitChance = 90;
				damage = 60;
				break;

			default:
				hitChance = 100;
				break;
		}

		Actions actions = new Actions();

		if (
				actor.getInventory().stream()
						.filter(item -> item instanceof Ammunition)
						.map(item -> (Ammunition) item)
						.anyMatch(item -> item.forWeapon(this))
		) {
			actions.add(new CancelAction());

			if (accuracyLevel < Sniper.MAX_ACCURACY) {
				actions.add(new SniperAimAction(this));
			}

			for (int x : map.getXRange()) {
				for (int y : map.getYRange()) {
					Actor target = map.at(x, y).getActor();

					if (target != null && target.hasCapability(ZombieCapability.UNDEAD)) {
						actions.add(new ShootAction(target, this, hitChance, damage, SHOOT_VERB));
					}
				}
			}
		} else {
			actions.add(new CancelAction("No ammunition"));
		}

		return actions;
	}

	@Override
	public void reset() {
		accuracyLevel = 0;
		chaser = 0;
	}

	@Override
	public void tick(Location currentLocation) {
		super.tick(currentLocation);
		reset();
		allowableActions.clear();
	}

	@Override
	public void tick(Location currentLocation, Actor actor) {
		super.tick(currentLocation, actor);

		if (allowableActions.size() == 0) {
			allowableActions.add(new SubMenuAction(this, "Use sniper"));
		}

		chaser += 1;
		
		if (accuracyLevel < chaser) {
			reset();
		}
	}

	/**
	 * Get the value corresponding to the ranking.
	 *
	 * @return integer value.
	 */
	@Override
	public int getRankingValue() {
		return RANKING.value;
	}
}
