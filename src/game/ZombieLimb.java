package game;

import edu.monash.fit2099.engine.*;

/**
 * A Zombie limb used as a weapon.
 */
public class ZombieLimb extends WeaponItem {

	private static final Ranking RANKING = Ranking.BRONZE;

	/**
	 * Type of Limb that fell off the Zombie.
	 */
	public enum Limb {ARM, LEG}
	private Limb limb;

	private final Item craftTo;

	/**
	 * Constructor.
	 * @param limb Limb that fell off the Zombie's body.
	 */
	public ZombieLimb(Limb limb) {
		super((limb == Limb.ARM) ? "Arm" : "Leg", '>', 15, "whacks");
		this.limb = limb;

		if (limb == Limb.ARM) {
			craftTo = new ZombieClub();
		} else {
			craftTo = new ZombieMace();
		}
		addCapability(Capable.FOR_SALE);
		addCapability(RANKING);
	}

	/**
	 * Get the Item that this crafts to
	 * @return The crafted Item
	 */
	public Item craft() {
		return craftTo;
	}

	/**
	 * Detect if it's on the ground
	 *
	 * @param currentLocation The location of the ground on which we lie.
	 */
	@Override
	public void tick(Location currentLocation) {
		super.tick(currentLocation);
		if (allowableActions.size() == 1) {
			allowableActions.clear();
		}
	}

	/**
	 * Detects if it's carried by an actor
	 *
	 * @param currentLocation The location of the actor carrying this Item.
	 * @param actor The actor carrying this Item.
	 */
	@Override
	public void tick(Location currentLocation, Actor actor) {
		super.tick(currentLocation, actor);
		if (allowableActions.size() == 0) {
			allowableActions.add(new CraftAction(this));
		}
	}

	/**
	 * Return a name of the limb
	 *
	 * @return string name of limb
	 */
	@Override
	public String toString() {
		return (limb == Limb.ARM ? "arm" : "leg");
	}

	/**
	 * Get the value corresponding to the ranking.
	 *
	 * @return integer value.
	 */
	@Override
	public int getRankingValue() {
		return RANKING.value;
	}
}
