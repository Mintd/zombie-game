package game;

import edu.monash.fit2099.engine.Item;
import edu.monash.fit2099.engine.Weapon;

/**
 * Ammunition for guns.
 */
public class Ammunition extends Item {

	private static final Ranking RANKING = Ranking.BRONZE;

	/**
	 * Type of gun this ammo is for.
	 */
	public enum AmmoType {
		SNIPER(Sniper.class),
		SHOTGUN(Shotgun.class);

		public final Class<?> weapon;

		private AmmoType(Class<?> weapon) {
			this.weapon = weapon;
		}
	}

	private AmmoType type;

	/**
	 * Constructor.
	 * 
	 * @param type Type of ammunition.
	 */
	public Ammunition(AmmoType type) {
		super(String.format("%s ammunition", type.toString().toLowerCase()), 'a', true);
		this.type = type;
		addCapability(Capable.FOR_SALE);
		addCapability(RANKING);
	}

	/**
	 * Checks if this ammo is usable in the given WeaponItem.
	 * 
	 * @param weapon weapon to check ammo compatibility.
	 * @return true if the ammo is usable in the weapon, false otherwise.
	 */
	public boolean forWeapon(Weapon weapon) {
		return type.weapon.isInstance(weapon);
	}

	/**
	 * Get the value corresponding to the ranking.
	 *
	 * @return integer value.
	 */
	@Override
	public int getRankingValue() {
		return RANKING.value;
	}
}
