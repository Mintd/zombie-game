package game;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.Display;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Menu;

/**
 * Action to show a submenu.
 */
public class SubMenuAction extends Action {
	private Menu menu = new Menu();
	private SubMenu item;
	private final String description;

	/**
	 * Constructor.
	 * 
	 * @param item Item that this submenu belongs to.
	 * @param description description to show to the player.
	 */
	public SubMenuAction(SubMenu item, String description) {
		this.item = item;
		this.description = description;
	}

	/**
	 * Show the submenu to the user.
	 * 
	 * @param player player the submenu is shown to.
	 * @param display the I/O object to which messages may be written.
	 * @param map map the player is on.
	 * @return Action that has been picked by the player.
	 */
	public Action showMenu(Player player, Display display, GameMap map) {
		return menu.showMenu(player, item.getMenuActions(player, map), display);
	}

	@Override
	public String execute(Actor actor, GameMap map) {
		throw new IllegalStateException("SubMenuAction executed");
	}

	@Override
	public String menuDescription(Actor actor) {
		return description;
	}
}
