package game;

import java.util.Optional;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;

/**
 * Class that generates a HealAction if the current actor needs healing
 */
public class HealBehaviour implements Behaviour {
	@Override
	public Action getAction(Actor actor, GameMap map) {
		Optional<Food> food = actor.getInventory()
				.stream()
				.filter(item -> item instanceof Food)
				.map(item -> (Food) item)
				.findFirst();

		if (food.isPresent()) {
			return new HealAction(food.get());
		} else {
			return null;
		}
	}
}
