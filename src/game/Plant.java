package game;

import edu.monash.fit2099.engine.Ground;
import edu.monash.fit2099.engine.Location;

/**
 * Abstract class representing plants.
 */
public abstract class Plant extends Ground {
	private int age = 0;
	private final int[] growthStages;
	private final char[] stageChars;
	private int currentStage = 0;

	/**
	 * Constructor.
	 * 
	 * @param growthStages Stages of the plant's growth.
	 * @param stageChars Display characters to use at each stage.
	 * @throws IllegalArgumentException If the length of growthStages isn't the same as stageChars.
	 */
	public Plant(int[] growthStages, char[] stageChars) {
		super(stageChars[0]);

		if (growthStages.length != stageChars.length) {
			throw new IllegalArgumentException(String.format(
				"Length of growthStages must be the same as the length of stageChars. %d != %d",
				growthStages.length, stageChars.length
			));
		}

		this.growthStages = growthStages.clone();
		this.stageChars = stageChars.clone();
	}

	@Override
	public void tick(Location location) {
		super.tick(location);
		grow(1);
	}

	/**
	 * Increases the plant's age and changes the display character accordingly.
	 * @param addAge Amount to increase age by.
	 */
	public void grow(int addAge) {
		if (isFullyGrown()) {
			return;
		}

		age += addAge;

		for (int stage = currentStage + 1; stage < growthStages.length; stage++) {
			if (age >= growthStages[stage]) {
				currentStage++;
				displayChar = stageChars[currentStage];
			} else {
				break;
			}
		}
	}

	/**
	 * Checks if the plant is fully grown.
	 *
	 * @return True if fully grown, False otherwise.
	 */
	public boolean isFullyGrown() {
		return age >= growthStages[growthStages.length - 1];
	}

	/**
	 * Returns the name of the plant.
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
