package game;

import java.util.Random;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actions;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Item;
import edu.monash.fit2099.engine.Weapon;

/**
 * Special Action for attacking other Actors.
 */
public class AttackAction extends Action {

    /**
     * The Actor that is to be attacked
     */
    protected Actor target;
    /**
     * Random number generator
     */
    protected Random rand = new Random();
    /**
     * Chance to hit the target
     */
    protected int hitChance = 50;
    /**
     * Weapon to use against target
     */
    protected Weapon weapon;
    /**
     * Damage to deal to target
     */
    protected int damage;
    /**
     * Verb to use
     */
    protected String verb;

    /**
     * Constructor.
     *
     * @param target the Actor to attack
     */
    public AttackAction(Actor target) {
        this.target = target;
    }

    @Override
    public String execute(Actor actor, GameMap map) {
        if (rand.nextInt(100) > hitChance) {
            return actor + " misses " + target + ".";
        }

        if (weapon == null) {
            weapon = actor.getWeapon();
            damage = weapon.damage();
            verb = weapon.verb();
        }

        String result = actor + " " + verb + " " + target;
        if (damage > 0) {
            result += " for " + damage + " damage";
        }
        result += ".";

        if (weapon.verb().equals("bites")) {
            actor.heal(5);
            result += String.format(" %s heals for 5 points.", actor);
        }

        target.hurt(damage);
        if (!target.isConscious() || damage == 0) {

            // check if the killer is a zombie
            Item corpse = new Corpse(
                    target.toString(),
                    actor.hasCapability(ZombieCapability.UNDEAD)
            );

            map.locationOf(target).addItem(corpse);

            Actions dropActions = new Actions();
            for (Item item : target.getInventory())
                dropActions.add(item.getDropAction());
            for (Action drop : dropActions)
                drop.execute(target, map);
            map.removeActor(target);

            result += System.lineSeparator() + target + " is killed.";
        } else if (target.hasCapability(Zombie.Capable.LIMB_LOSS)) {
            result += String.format(" %s loses limb.", target);
        }

        return result;
    }

    @Override
    public String menuDescription(Actor actor) {
        return actor + " attacks " + target;
    }
}
