package game;

import java.util.Optional;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Item;
import edu.monash.fit2099.engine.PickUpItemAction;

/**
 * Returns a PickUpItemAction if there is an item of the desired type on the ground.
 */
public class PickUpItemBehaviour implements Behaviour {
	private final Class<?> desiredItem;
	private final long desiredCount;

	/**
	 * Constructor.
	 *
	 * @param desiredItem Type of item to pick up.
	 * @param desiredCount Number of items to keep.
	 * @throws IllegalArgumentException If desiredCount is not more than 0.
	 */
	public PickUpItemBehaviour(Class<?> desiredItem, long desiredCount) {
		this.desiredItem = desiredItem;

		if (desiredCount > 0) {
			this.desiredCount = desiredCount;
		} else {
			throw new IllegalArgumentException(
					String.format("desiredCount must be larger than 0: received %d", desiredCount)
			);
		}
	}

	@Override
	public Action getAction(Actor actor, GameMap map) {
		long currentCount = actor.getInventory()
				.stream()
				.filter(desiredItem::isInstance)
				.limit(desiredCount)
				.count();

		if (currentCount >= desiredCount) {
			return null;
		}

		Optional<Item> item = map.locationOf(actor).getItems()
				.stream()
				.filter(desiredItem::isInstance)
				.findAny();

		if (item.isPresent()) {
			return new PickUpItemAction(item.get());
		} else {
			return null;
		}
	}
}
