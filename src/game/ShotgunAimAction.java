package game;

import java.util.Arrays;
import java.util.stream.Collectors;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.NumberRange;

/**
 * Action for aiming a shotgun in a particular direction.
 */
public class ShotgunAimAction extends Action {
	private boolean consumeAmmo = true;
	private Shotgun shotgun;
	private ShotgunShootDirection direction;
	private int x, y;

	/**
	 * Constructor.
	 * 
	 * @param shotgun shotgun to use.
	 * @param direction direction to fire in.
	 * @param x x coordinate of the player.
	 * @param y y coordinate of the player.
	 */
	public ShotgunAimAction(Shotgun shotgun, ShotgunShootDirection direction, int x, int y) {
		this.shotgun = shotgun;
		this.direction = direction;
		this.x = x;
		this.y = y;
	}

	@Override
	public String execute(Actor actor, GameMap map) {
		NumberRange xRange = map.getXRange();
		NumberRange yRange = map.getYRange();

		return Arrays.stream(direction.getAffectedLocations(x, y))
				.filter(point -> xRange.contains(point.x) && yRange.contains(point.y))
				.filter(point -> map.at(point.x, point.y).getActor() != null)
				.map(point ->
						new ShootAction(
								map.at(point.x, point.y).getActor(),
								shotgun,
								Shotgun.HIT_CHANCE,
								Shotgun.DAMAGE_DEALT,
								Shotgun.SHOOT_VERB,
								useAmmo()
						).execute(actor, map)
				)
				.collect(Collectors.joining(System.lineSeparator()));
	}

	private boolean useAmmo() {
		if (consumeAmmo) {
			consumeAmmo = !consumeAmmo;
			return true;
		}

		return consumeAmmo;
	}

	@Override
	public String menuDescription(Actor actor) {
		return String.format("%s fires %s", actor, direction.fullName);
	}
}
