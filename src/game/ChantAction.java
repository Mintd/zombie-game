package game;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;

import java.util.Random;

/**
 * This action will create five new Zombies at random locations on the current map.
 */
public class ChantAction extends Action {

    private Random rand = new Random();
    /**
     * Number of copy represents the number of time this action has been called
     */
    private int copy;

    /**
     * Constructor.
     *
     * @param copy integer value represents the number of time this action has been called
     */
    public ChantAction(int copy) {
        this.copy = copy;
    }

    /**
     * Creates five new Zombies at random locations on the current map.
     *
     * @param actor The actor performing the action.
     * @param map The map the actor is on.
     * @return string description on the action.
     */
    @Override
    public String execute(Actor actor, GameMap map) {
        // get the name of the Zombies
        String[] names = createNames(copy);
        // creates five new Zombies
        Zombie[] zombies = new Zombie[5];
        for (int i = 0; i < zombies.length; i++) {
            zombies[i] = new Zombie(names[i]);
        }
        // get random locations on the current map
        for (Zombie zombie: zombies) {
            int x;
            int y;
            do {
                x = rand.nextInt(map.getXRange().max());
                y = rand.nextInt(map.getYRange().max());
            } while (map.at(x, y).containsAnActor());

            // put the Zombie on the map
            map.at(x, y).addActor(zombie);
        }
        return String.format("%s creates Zombies.", actor);
    }

    /**
     * Returns a string description of the action.
     *
     * @param actor The actor performing the action.
     * @return string description of the action.
     */
    @Override
    public String menuDescription(Actor actor) {
        return String.format("%s creates Zombies.", actor);
    }

    /**
     * Return an array of String containing five distinct names along with the number of copy.
     *
     * @param copy number of copy
     * @return an array of String.
     */
    private String[] createNames(int copy) {
        String[] names = {
                "Molly",
                "Addy",
                "Abby",
                "Juris",
                "Moris"
        };

        for (int i = 0; i < names.length; i++) {
            names[i] = names[i] + copy;
        }

        return names;
    }

}
