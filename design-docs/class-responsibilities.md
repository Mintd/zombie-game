# Class Responsibilities

## ActorInterface

- Default methods for setting and getting the credit point

## Ammunition

- Keeps track of which weapon it is for

## AttackAction

- Add dead human to be an instance of Corpse
- Heal Zombie if it uses a bite attack on a Human

## ChantAction

- Creates 5 Zombies at random locations on the map

## CancelAction

- Used for exiting out of submenus
- Contains a description that it shows to the user in the menu

## CraftAction

- Replaces the original Item with a new Item, and places it into the
  player's inventory

## Corpse

- Keep track of the turns, and instantiate a Zombie between 5 to 10 turns with a certain amount of probability

## Crop

- Knows how much to increase its age when fertilized
- Returns a HarvestAction if it can be harvested
- Knows what food can be harvested from it
- Keeps track of whether it has been damaged

## Farmer

- Store FarmBehaviour and uses it to determine how to play the turn

## FarmBehaviour

- If the Actor is part of the undead team, damage crops instead, which prevents fertilization.
- Scans the surrounding area to determine if there are plants to harvest
- Scans the surrounding area to determine if there are plants to fertilize
- Plants Crops

## FarmAction

- Performs the specified action that FarmBehaviour determined

## Food

- Knows how much to heal an actor by

## GameMapPlus

- Has a Voodoo static instance variable
- Add and remove Voodoo on the map in tick method

## Human

- Has a HealBehaviour when it eats the harvested crop
- Has a PickUpItemBehaviour that is set to pick up Food

## HarvestAction

- Removes the Crop from the map
- Drops the Food onto the ground, or in the player's inventory

## HealAction

- Calls the heal method on the Actor to heal them by the amount specified in the Food

## HealBehaviour

- Determines if an Actor should heal, and returns a HealAction if it can

## ItemInterface

- Has a enum of Capable for selling and Ranking with associated value.

## WorldPlus

- Handles QuitException, and gives the user a goodbye message
- Knows when the game should end, and whether is is a loss or a win

## PickUpItemBehaviour

- Stores the type of Item that it should look for
- Stores the number of said Item that the actor should have in its' inventory
- Searches the surrounding tiles for Items matching the type, and returns
  PickUpItemAction

## Plant

- Stores the growth stages of the plant
- Stores the display characters
- Increments age every turn

## QuitAction

- Throws a QuitException when called

## Vehicle

- Teleport player between maps

## Voodoo

- Performs ChantAction to create Zombies at random locations every 10 turns

## Player

- Displays submenus to the user

## Point

- Stores the x and y coordinates of a particular point on the map

## PortableLimbItem

- Has a DropLimbItemAction that drops ZombieLimb at random location

## PurchaseAction

- Adds Item to Actor's inventory and deducts credit for the Actor.

## Resettable

- Interface
- Marks an object as something that needs to be reset when the player gets hit

## SellAction

- Removes Item from Actor's inventory and adds credit for the Actor

## Shop

- Able to Sell, Purchase Item from the Actor

## ShootAction

- Stores the target Actor, weapon, hit chance, damage, and verb to use for the attack
- Removes ammunition from the Actor's inventory

## Shotgun

- Ranged weapon
- Stores the damage that it should deal, along with the hit chance and verb
- Populates its submenu

## ShotgunAimAction

- Knows the direction in which the player is aiming
- Knows the coordinates of the player
- Checks locations and applies a ShootAction onto any areas with an attackable Actor

## ShotgunShootDirection

- Returns a list of Points corresponding to the Locations where a ShootAction can be applied

## Sniper

- Ranged weapon
- Stores the damage that it should deal, along with the hit chance and verb
- Populates its submenu
- Stores the accuracy level

## SniperAimAction

- Keeps track of the sniper it's connected to
- Calls the aim() method of the sniper when executed

## SubMenu

- Interface
- Signals that an object has a submenu that can be displayed

## SubMenuAction

- Displays the submenu
- Calls the getMenuActions method of a SubMenu to get the menu items

## Zombie

- Decides which unarmed attack it uses
- Checks items to see if they are instances of WeaponItem so that it
  can pick it up
- Says "Braaaaains" and other zombie words
- Checks its inventory to see if it still has limb to perform certain action
- Drops its limb at random location which become an instance of ZombieLimb

## ZombieLimb

- Knows the amount of damage it should deal
- Knows what it crafts into
- Perform CraftAction when it is carried by an actor (player)

## ZombieClub / ZombieMace

- Knows the amount of damage it should deal
