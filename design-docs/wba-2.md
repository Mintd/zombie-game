# Work Breakdown Agreement

## Deliverables

- Expand Zombie attacks and actions
  - Main reviewer: Wong Yi Xiong

- Zombie should take damage and fall aparts
  - Main reviewer: Lee Yu-Huei

- Craft weapons
  - Main reviewer: Wong Yi Xiong

- Humans killed by Zombies should become Zombies themselves
  - Main reviewer: Lee Yu-Huei

- Farmers should plant crops, and harvest it for food
  - Main reviewer: Wong Yi Xiong

All deliverables will be produced equally by all team members, and the main
reviewer is expected to check that their assigned diagrams are correct.

I accept this WBA
I accept this WBA
