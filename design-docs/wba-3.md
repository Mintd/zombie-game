# Work Breakdown Agreement

## Deliverables

- Going to town
  - Main reviewer: Lee Yu-Huei

- New weapons: shotgun and sniper rifle
  - Main reviewer: Wong Yi Xiong

- Mambo Marie
  - Main reviewer: Lee Yu-Huei

- Ending the game
  - Main reviewer: Wong Yi Xiong

All deliverables will be produced equally by all team members, and the main
reviewer is expected to check that their assigned diagrams are correct.


I accept this WBA
I accept this WBA
