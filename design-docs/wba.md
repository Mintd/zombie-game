# Work Breakdown Agreement

## Deliverables

- Class diagram
  - Main reviewer: Lee Yu-Huei
- Interaction diagram (sequence or communication)
  - Main reviewer: Wong Yi Xiong
- Design rationale
  - Both team members are in charge of reviewing this document

All deliverables will be produced equally by all team members, and the main
reviewer is expected to check that their assigned diagrams are correct.

I accept this WBA
I accept this WBA