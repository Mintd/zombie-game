# Design Rationale

## Zombie Attacks

- Since getIntrinsicWeapon is used when the Actor is unarmed, I decided that
  this is the appropriate place for a new unarmed attack. A side effect of this
  is that a zombie will not be able to bite if it is carrying a weapon.

- I decided that zombie healing should occur in the execute() method of
  AttackAction, as it is that method that decides if an attack lands.

- A new behaviour called PickUpItemBehaviour checks the surrounding tiles for
  weapons that the actor can pick up. This follows the DRY principle as Humans
  also have the same behaviour.

- The zombie sounds should be done within playTurn as it has a Display
  parameter that can be used to output their sounds.

## Attacking Zombies

- Override the hurt() method so that when the zombie gets hurt, it will have 25% chance of losing one of its limbs by removing the limb item from the zombie's inventory. The limb will be of type PortableItem class. Because the limbs are a subclass of PortableItem, we can use the existing removeItemFromInventory() method to remove the limb from Zombie's inventory. This is the **DRY** principle.

- Since when each zombie is created, it will have two arms and two legs; we can add the limbs item to zombie's inventory during creation in order to keep track of the amount of limbs the zombie has throughout the game.

- When a zombie's arm is lost, we need to reduce the probability of punching. This can be done by reducing the chance of returning a punch in the getIntrinsicWeapon() method by half. If the zombie loses both arms, we need to let the zombie drops all its weapons by calling dropWeapon() in dropLimb() method to drop all the weapons as well as arms the zombie has in its inventory.

- If zombie loses one leg, remove leg item from zombie's inventory and modify the playTurn() method so it checks how many leg item left inside zombie's inventory. If the zombie has only one leg, then it can only perform the HuntBehaviour and WanderBehaviour every two turns.

- If zombie loses both legs, it will no longer has the HuntBehaviour and WanderBehaviour, since when playTurn() method is called, it will check if the zombie has the required limb to perform the two behaviours by calling satisfiedCondition() method.

- When a zombie loses limb, the limb becomes a new type of WeaponItem named ZombieLimb, it is being done in the dropLimb() method to create the new weapon and adds it to the map.

## Crafting Weapons

- ZombieLimbs have a CraftAction that creates a new WeaponItem (ZombieClub or
  ZombieMace, depending on the parameter passed to the constructor) and places
  it in the player's inventory. It was done this way as all available Actions
  are shown to the player. This is the **DRY** principle as lose limb is also a simple club
  and will need to able to do some damage, hence the ZombieLimb inherits the WeaponItem which has all the features required.

- The CraftAction's execute() method will call a craft method in the ZombieLimb class that returns the new weapon that it should craft into.
  This way each item can keep track of what item they can be crafted into.

## Rising from the dead

- When a human gets killed, it will be removed from the map.
- At the human's location, an instance of Corpse will be created and added to the map.
- The Corpse class inherits PortableItem class so it can uses the tick() method to keep track of the turns in the game.

  - The corpse has the ability to rise from dead if the actor who performs the attack is a zombie. To do so, we need to call hasCapability() method to check if it has a capability of UNDEAD. When the tick() method returns 5, it has a certain amount of probability to become a zombie by removing the corpse item and instantiate a zombie. As long as the corpse has not yet become a zombie, the probability increases when the tick inclines. When the tick reaches 10, it will eventually become a zombie.
  - If corpse is being carried by an actor, when corpse becomes zombie, it will be placed at the adjacent tiles else just placed at the current location.

## Farmers and food

- HealBehaviour will be created. This will make Humans eat food if they are not
  at full health. I chose to make this new behaviour as both Humans and Farmers
  should do the same thing. This reduces code duplication.

- FarmBehaviour will be created. This behaviour is in charge of planting crops,
  fertilizing, and harvesting them.

- A new class Farmer that inherits from Human, with a FarmBehaviour that causes
  it to (in this order):

  - Harvest crops that can be harvested
  - Fertilize Crops that are not fully grown
  - Plant crops if it's standing on dirt

- A new class Food, which inherits from PortableItem as it can be picked up by
  Actors, with a HealAction that heals an actor by a certain amount. I did not
  make this an abstract class with a class inheriting from it as we only have
  one type of food at this point in time. If more food types are added then
  this will be changed.

- A new abstract class Plant that contains the logic needed for all plants in
  the world. This reduces code duplication between classes like Crop and Tree.

- A new class Crop that inherits from Plant.

- Crop will also have an Action called HarvestAction when it reaches its set
  age, which will allow players to harvest the crop, along with setting a
  growth capability.

- HarvestAction will have an execute method that calls the harvest() method on
  a crop, causing it to drop the food on the ground if the Actor is not a player.

## Going to town

- Town map is created in the Application the same way as the game map.

  - Actors appear in the town map includes Human, Zombie, Farmer.

  - Items such as Vehicle, Fence, Tree, Sinper and Shotgun is placed on the town map as well.

- Ammunition are placed on both compound and town map.

  - At a random location, there will be multiple ammunition being placed at the same spot. These ammunition are created in an array list first then added one by one at the same location on the map.

- Vehicle is created and inherits from Item.

  - MoveActorAction is added to allowableActions during creation which can teleport player to other map.

  - Vehicle is created in Application and placed in both town and game map.

## Mambo Marie

- Voodoo is created and inherits from ZombieActor.

  - At each play turn, Voodoo will perform WanderBehaviour or DoNothingAction if the one before is not available.

  - Voodoo has a counter that records the number of play turn.

  - Voodoo also has a enum Capable which is VANISH.

    - VANISH is added to its capability when the counter reaches 30 and is removed when Voodoo disappear from the map,
    this is done in the tick() method of the subclass of GameMap.

  - When the counter reaches 10, Voodoo will perform ChantAction.

- ChantAction is created and inherits from Action.

  - In the execute() method, it creates five Zombies and placed them on the map at random location.

  - It has a private method createNames() to generate Zombies' names with a suffix indicating the number of copy for the Zombie being created.

- GameMapPlus is created and inherits from GameMap.

  - tick() method is override in order to create Voodoo with 5 percent chance when Voodoo is still conscious.

  - When Voodoo is created, it is placed at the edge of the map.

  - tick() method removed Voodoo from the map when Voodoo VANISH.

## Submenus

- Implemented by using a SubMenuAction, and implementing special handler code
  in Player that will handle instances of this object.

- CancelAction, which allows a user to exit a submenu, leading back to the main
  menu.

- This implementation allows us to use the existing Menu class with no
  modifications required.

## Ranged Weapons

- Ammunition will be created, and it knows what weapon it is compatible with.
  I did this so that I would not have to make a class for every weapon that
  gets added to the game.

- New weapons will be created: Shotguns and Snipers.

- These new weapons will know their hit chances and damage.

- A new Point class, that contains the x and y coordinates of a point on the
  map.

- A ShootAction that inherits from AttackAction, which reduces code repetition
  as all the other features implemented in AttackAction can be used for free.

## Ending the game

- To quit the game, an exception is used as this will immediately stop other execution and
  propagate up to WorldPlus to handle

- WorldPlus checks game states to see if the game should still be running. This check is done here
  as the World class already has a stillRunning method so I decided that this was the best place to
  put the new checks.

# Bonus Feature

## Shop sells new ranged weapons and ammunition

- Shop class is created.

  - Shop inherits from Item class and implements SubMenu since it needs to make use of getSubMenuActions to list out the items for actor to purchase, and show the item that the actor can sell in exchange for credit.
  - Shop has an array list of Item which contains shotguns, sniper rifles, and ammunition.
  - Shop overrides the getMenuActions() method. There are three action that Shop can performs: CancelAction, SellAction, PurchaseAction.

    - SellAction is added to actions when the Actor has Item that has the capability FOR_SALE in its inventory.
    - PurchaseAction is added when Actor has enough credit to purchase the Item.
    - CancelAction is added when the 4 situations occurs:

      - For Actor to cancel selling or purchasing.
      - Actor has no Item for sale with description "No item for sale".
      - Actor has no enough credit to purchase with description "Not enough credit to purchase"
      - Shop is out of stock with description "Shop out of stock".

    - Shop overrides tick() method.

      - Add SubMenuAction with the description "Enter Shop" to allowableActions if there's an Actor at that location.

- ItemInterface has enum Capable and Ranking.

  - Each sellable Weapon and Item has a FOR_SALE capability added during creation.
  - Each sellable Weapon and Item has a GOLD/ SILVER/ BRONZE/ PLATINUM capability added during creation.
  - ItemInterface has a default method getRankingValue() that is used to get the value associated to the Ranking. Using default method prevents classes that extends Item from implementing the method not necessary for it.

- Ranking enum class is created in ItemInterface.

  - Enum type: GOLD, SILVER, BRONZE, WOOD.
  - Each type has a value associated with it. GOLD is 9, SILVER is 3, BRONZE is 2, WOOD is 1.

- ActorInterface has two methods: setter and getter for credit.

  - Player has a credit attribute to store the credit point it has.
  - Player overrides the two methods. Setter sets the credit point to the Player's credit attribute and getter gets the value of the credit attribute from the Player.
  - Implementing the methods in the interface prevents downcasting.

- SellAction is created.

  - SellAction inherits from Action class.
  - SellAction takes in the parameter Item from Actor.
  - SellAction adds credit points to the Actor and removes the Item from Actor's inventory in the execute() method.

- PurchaseAction is created.

  - PurchaseAction inherits from Action class.
  - PurchaseAction takes in parameter the Shop and the item to purchase.
  - PurchaseAction removes the Item from Shop and adds it to Actor's inventory and decrement credit points the Actor has.

## Zombies damage crops

- FarmBehaviour is modified to find crops to destroy if the actor is part of
  the undead team. I did it this way to keep all Crop related logic inside
  one Behaviour.

- FarmAction is modified to damage a crop. This has been done for the same
  reason as above.
